package com.springboot.EmployeeRestAPI.Repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.EmployeeRestAPI.Model.Employee;

@Transactional
public interface EmployeeRepository extends JpaRepository<Employee, Long>
{
  List<Employee> findAll();
  Employee findById(int id);
  
  void deleteById(int id);

 }
