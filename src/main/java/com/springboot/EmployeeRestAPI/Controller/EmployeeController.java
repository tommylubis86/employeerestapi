package com.springboot.EmployeeRestAPI.Controller;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import com.springboot.EmployeeRestAPI.Model.Employee;

import com.springboot.EmployeeRestAPI.Repository.EmployeeRepository;


@Controller
public class EmployeeController {

	@Autowired 
	private EmployeeRepository employeeRepository; 
	
	
	
	//============ update Employee ================//
	@RequestMapping(value = { "/employee/{id}" },
		    		method = RequestMethod.POST, 
		    		consumes = MediaType.APPLICATION_JSON_VALUE
		    		)
	public ResponseEntity<?> updateEmployee(@RequestBody Employee paramEmployee ,@PathVariable("id") int id) 
	{
	     Employee savedEmployee = employeeRepository.findById(id);
	     
	     if(savedEmployee == null) {
	    	 return new ResponseEntity<String>("NOT FOUND",HttpStatus.NOT_FOUND);	 
	     } else {
	    	 try {
	    	 savedEmployee.setFull_name(paramEmployee.getFull_name());
	    	 savedEmployee.setRole_id(paramEmployee.getRole_id());
	    	 savedEmployee.setRole_name(paramEmployee.getRole_name());
	    	 savedEmployee.setAddress(paramEmployee.getAddress());
	    	 savedEmployee.setDob(paramEmployee.getDob());
	    	 savedEmployee.setSalary(paramEmployee.getSalary());
	    	 
	    	 employeeRepository.save(savedEmployee);
	    	 	
	    	 return new ResponseEntity<String>("SUCCESS",HttpStatus.ACCEPTED);
	    	 	
	    	 } catch(Exception exc)
	    	 {
	    		 return new ResponseEntity<String>("Error ---> " + exc.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
	    	 } 
	     }	
	}

	
	//============ Insert new Employee ================//
	@RequestMapping(value = { "/employee" },
    		method = RequestMethod.PUT, 
    		consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> InsertEmployee(@RequestBody Employee paramEmployee) 
	{
		Employee newEmployee = new Employee();
  
	 try {
		 newEmployee.setFull_name(paramEmployee.getFull_name());
		 newEmployee.setRole_id(paramEmployee.getRole_id());
		 newEmployee.setRole_name(paramEmployee.getRole_name());
		 newEmployee.setAddress(paramEmployee.getAddress());
		 newEmployee.setDob(paramEmployee.getDob());
		 newEmployee.setSalary(paramEmployee.getSalary());
	 
		 Employee createdEmployee = employeeRepository.save(newEmployee);
	 	
		 return new ResponseEntity<Employee>(createdEmployee,HttpStatus.OK);
	 	
	 } catch(Exception exc)	 {
		 return new ResponseEntity<String>("Error ---> " + exc.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
	 }
	}




@RequestMapping(value = {"/employee","/employee/{id}" },
				method = RequestMethod.GET)
private ResponseEntity<?> getEmployee(@PathVariable(name = "id",required = false) String paramId) 
{
    if(paramId == null) {
		 
	   List<Employee> listEmployee = employeeRepository.findAll();	 
	   return new ResponseEntity<List<Employee>>(listEmployee,HttpStatus.OK);
	 
	 } else { 
		 
		 int employeeId = Integer.parseInt(paramId);
		 Employee existingEmployee = employeeRepository.findById(employeeId);
		 
		 return new ResponseEntity<Employee>(existingEmployee,HttpStatus.OK);		 
	 }
	
}


@RequestMapping(value = {"/employee/{id}"},
			    method = RequestMethod.DELETE)
private ResponseEntity<?> deleteEmployee(@PathVariable(name = "id",required = true) String paramId) 
{
	boolean isSuccess = false;
	String errMessage = "";
    try { 
    	
    	int employeeId = Integer.parseInt(paramId);
    	
    	employeeRepository.deleteById(employeeId);
    	
    	isSuccess = true;
        
    } catch(Exception exc) {    	
    	System.out.println("Error --> " + exc.getMessage());
    	isSuccess = false;
    	errMessage = exc.getMessage();
    }
    
    if(isSuccess == true ) { 
    	return new ResponseEntity<String>("SUCCESS",HttpStatus.ACCEPTED);
    } else {
    	return new ResponseEntity<String>(errMessage,HttpStatus.INTERNAL_SERVER_ERROR);
    }
}




}
